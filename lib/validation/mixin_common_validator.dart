mixin CommonValidation{
  String? validateEmail(String? value){
    if(!value!.contains("@") && !value!.contains(".")){
      return "Please input valid email";
    }
    return null;
  }

  String? validatePassword(String? value){
    RegExp regex =
    RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');
    if (value!.isEmpty) {
      return 'Please enter password';
    }
    else {
      if (!regex.hasMatch(value)) {
        return 'Password phải có 8 ký tự, bao gồm chữ hoa, thường, số và ký tự đặc biệt';
      }
      else {
        return null;
      }
    }
  }

  String? validateName(String? value){
    final nameRegExp = new RegExp(r"^\s*([A-Za-z]{1,}([\.,] |[-']| ))+[A-Za-z]+\.?\s*$");
    if (value!.isEmpty) {
      return 'Please enter name';
    }
    if(!nameRegExp.hasMatch(value!)){
      return "Tên không được có các ký tự đặc biệt và số";
    }
    return null;
  }

  String? validateAddress(String? value){
    if (value!.isEmpty) {
      return 'Please enter address';
    }
    return null;
  }

  String? validatePhone(String? value){
    final nameRegExp = new RegExp(r"^\+?0[0-9]{10}$");
    if (value!.isEmpty) {
      return 'Please enter phone';
    }
    if(!nameRegExp.hasMatch(value!)){
      return "SĐT chỉ được ghi số";
    }
    return null;
  }

}